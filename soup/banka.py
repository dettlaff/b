from aiohttp import ClientSession
from logging import basicConfig, DEBUG

from aiogram import executor
from aiogram import Bot, Dispatcher
from aiogram.types import ParseMode
from aiogram.types import Message, CallbackQuery
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from aiogram.dispatcher.filters import CommandStart


API_URL = "https://free.navalny.com/api/v1/maps/counters/"

BOT_TOKEN = "token"
BOT_OWNER = [id]

bot = Bot(token=BOT_TOKEN, parse_mode=ParseMode.HTML)
dp = Dispatcher(bot)


async def on_startup_notify(dp: Dispatcher):
    for i in BOT_OWNER:
        await bot.send_message(i, "started.")


def create_session():
    """
    Создание новой сессии
    """
    return ClientSession(
        headers={
            "User-Agent": "bot"
        }
    )



async def get_count():

    session = create_session()

    response = await session.get(API_URL)
    query = await response.json()
    await session.close()

    return int(query["persons"])


def get_keyboard():

    markup = InlineKeyboardMarkup()

    markup.add(
      InlineKeyboardButton(
        text="Обновить", callback_data="update_info"
      )
    )

    return markup



@dp.message_handler(CommandStart())
async def check_count_of_members(message: Message):

    count = await get_count()

    await message.reply(
        text=(
            f"Количество участников акции: <b>{count}</b>"
        ),
        reply_markup=get_keyboard()
    )



@dp.callback_query_handler(text="update_info")
async def update_info(call: CallbackQuery):

    message = call.message

    new_count = await get_count()
    old_count = int(message.text.split()[3])

    if old_count == new_count:
        return await call.answer(
            text=(
                "Количество участников не изменилось"
            ),
            show_alert=True
        )

    await message.edit_text(
        text=(
            f"Количество участников акции: <b>{new_count}</b>"
            f"\nСтало больше на: {new_count - old_count}"
        ),
        reply_markup=get_keyboard()
    )

    await call.answer()



if __name__ == "__main__":
    executor.start_polling(dp, on_startup=on_startup_notify)
